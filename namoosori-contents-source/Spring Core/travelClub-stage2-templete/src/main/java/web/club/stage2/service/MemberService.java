package web.club.stage2.service;

import web.club.stage2.entity.club.CommunityMember;
import web.club.stage2.service.sdo.MemberCdo;
import web.club.stage2.util.helper.NameValueList;

import java.util.List;

public interface MemberService {
	//
	String registerMember(MemberCdo member);
	CommunityMember findMemberById(String memberId);
	CommunityMember findMemberByEmail(String memberEmail);
	List<CommunityMember> findMembersByName(String name);
	void modifyMember(String memberId, NameValueList member);
	void removeMember(String memberId);
}