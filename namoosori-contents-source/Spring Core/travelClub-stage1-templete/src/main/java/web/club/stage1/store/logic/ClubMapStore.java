package web.club.stage1.store.logic;

import org.springframework.stereotype.Repository;
import web.club.stage1.entity.club.TravelClub;
import web.club.stage1.store.ClubStore;

import java.util.*;

public class ClubMapStore implements ClubStore {
	@Override
	public String create(TravelClub club) {
		return null;
	}

	@Override
	public TravelClub retrieve(String clubId) {
		return null;
	}

	@Override
	public List<TravelClub> retrieveByName(String name) {
		return null;
	}

	@Override
	public List<TravelClub> retrieveAll() {
		return null;
	}

	@Override
	public void update(TravelClub club) {

	}

	@Override
	public void delete(String clubId) {

	}

	@Override
	public boolean exists(String clubId) {
		return false;
	}
}
