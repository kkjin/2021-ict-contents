package web.club.stage1.service.logic;

import web.club.stage1.entity.club.TravelClub;
import web.club.stage1.service.ClubService;
import web.club.stage1.service.sdo.TravelClubCdo;
import web.club.stage1.util.helper.NameValueList;

import java.util.List;

public class ClubServiceLogic implements ClubService {
	@Override
	public String registerClub(TravelClubCdo club) {
		return null;
	}

	@Override
	public TravelClub findClubById(String id) {
		return null;
	}

	@Override
	public List<TravelClub> findClubsByName(String name) {
		return null;
	}

	@Override
	public List<TravelClub> findAllTravelClubs() {
		return null;
	}

	@Override
	public void modify(String clubId, NameValueList nameValues) {

	}

	@Override
	public void remove(String clubId) {

	}
}
