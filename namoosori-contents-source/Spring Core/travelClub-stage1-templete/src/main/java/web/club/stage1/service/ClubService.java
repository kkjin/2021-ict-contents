package web.club.stage1.service;

import web.club.stage1.entity.club.TravelClub;
import web.club.stage1.service.sdo.TravelClubCdo;
import web.club.stage1.util.helper.NameValueList;

import java.util.List;

public interface ClubService {
	//
	String registerClub(TravelClubCdo club);
	TravelClub findClubById(String id);
	List<TravelClub> findClubsByName(String name);
	List<TravelClub> findAllTravelClubs();
	void modify(String clubId, NameValueList nameValues);
	void remove(String clubId);
}
