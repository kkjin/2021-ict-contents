package web.club.stage1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TravelClubApplication {
    public static void main(String[] args) {
        SpringApplication.run(TravelClubApplication.class, args);
    }
}
