import TravelClub from '../../step1/entity/TravelClub';


class MapStorage {
    //
    clubMap: Map<string, TravelClub>;
    static uniqueInstance: MapStorage;

    constructor() {
      //
      this.clubMap = new Map<string, TravelClub>();
    }

    static getInstance(): MapStorage {
      //
      if (!this.uniqueInstance) {
          this.uniqueInstance = new MapStorage();
      }
      return this.uniqueInstance;
    }

}

export default MapStorage;
